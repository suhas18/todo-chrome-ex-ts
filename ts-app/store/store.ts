import { configureStore, getDefaultMiddleware } from '@reduxjs/toolkit';
import { TypedUseSelectorHook, useDispatch, useSelector } from 'react-redux';
import logger from 'redux-logger';
import todosReducer, { TodosState}  from './todosSlice';

const middleware = [...getDefaultMiddleware(), logger];

const store = configureStore({
  reducer: {
    todos: todosReducer,
  },
  devTools: process.env.NODE_ENV !== 'production',
  middleware,
});

export type AppDispatch = typeof store.dispatch;
export const useAppDispatch = () => useDispatch<AppDispatch>();
export type RootState = {
  todos: TodosState;
};
export const useTypedSelector: TypedUseSelectorHook<RootState> = useSelector;

export default store;
