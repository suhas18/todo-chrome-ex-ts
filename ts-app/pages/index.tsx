import { useState } from "react";
import { useDispatch, useSelector } from "react-redux";
import {
  addTodo,
  toggleTodoCompleted,
  deleteTodo,
  selectTodos,
} from "../store/todosSlice";
import { RootState } from "../store/store";
import { v4 as uuidv4 } from 'uuid';

export default function Home() {
  const dispatch = useDispatch();
  const todos = useSelector((state: RootState) => selectTodos(state));
  const [inputValue, setInputValue] = useState("");

  function handleInputChange(event: React.ChangeEvent<HTMLInputElement>) {
    setInputValue(event.target.value);
  }

  function handleAddTodo() {
    if (inputValue.trim()) {
      dispatch(
        addTodo({
          id: uuidv4(),
          title: inputValue.trim(),
          completed: false,
        })
      );
      setInputValue("");
    }
  }

  function handleToggleTodoCompleted(id: number) {
    dispatch(toggleTodoCompleted(id));
  }

  function handleDeleteTodo(id: number) {
    dispatch(deleteTodo(id));
  }

  return (
    <div className="bg-gray-100  flex flex-col justify-center items-center w-auto min-h-screen rounded">
      <div className="bg-white rounded-lg shadow-lg p-6">
        <h1 className="text-3xl font-bold mb-6">Todo List</h1>
        <div className="flex mb-6">
          <input
            className="rounded-l-lg border-gray-400 py-2 px-4 mr-0 leading-tight focus:outline-none"
            type="text"
            value={inputValue}
            onChange={handleInputChange}
          />
          <button
            className="bg-blue-500 hover:bg-blue-700 text-white font-bold py-2 px-4 rounded-r-lg"
            onClick={handleAddTodo}
          >
            Add Todo
          </button>
        </div>
        <ul className="list-disc list-inside">
          {todos.map((todo) => (
             <li
             key={todo.id}
             className={`flex items-center justify-between py-2`}
             style={{borderWidth:1,borderColor:"gray",padding:5,margin:5,borderRadius:10}}
           >
              <div className="flex items-center">
                <input
                  className="mr-2 leading-tight"
                  type="checkbox"
                  checked={todo.completed}
                  onChange={() => handleToggleTodoCompleted(todo.id)}
                />

                <span
                  className={
                    todo.completed
                      ? "line-through text-gray-400"
                      : "text-gray-800"
                  }
                >
                  {todo.title}
                </span>
              </div>
              <button
                className="bg-red-500 hover:bg-red-700 text-white font-bold py-1 px-2 rounded-lg"
                onClick={() => handleDeleteTodo(todo.id)}
              >
                Delete
              </button>
            </li>
          ))}
        </ul>
      </div>
    </div>
  );
}
